class CreateEtudes < ActiveRecord::Migration[5.1]
  def change
    create_table :etudes do |t|
      t.string :SecteurEtude
      t.string :Niveau
      t.datetime :DateDebut
      t.datetime :DateComplition
      t.references :Client_id, foreign_key: true
      t.references :Institution_id, foreign_key: true

      t.timestamps
    end
  end
end
