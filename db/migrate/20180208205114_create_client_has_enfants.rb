class CreateClientHasEnfants < ActiveRecord::Migration[5.1]
  def change
    create_table :client_has_enfants do |t|
      t.references :Enfant_id, foreign_key: true
      t.references :Client_id, foreign_key: true
      t.integer :Lien

      t.timestamps
    end
  end
end
