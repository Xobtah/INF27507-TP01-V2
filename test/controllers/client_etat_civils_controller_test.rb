require 'test_helper'

class ClientEtatCivilsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @client_etat_civil = client_etat_civils(:one)
  end

  test "should get index" do
    get client_etat_civils_url
    assert_response :success
  end

  test "should get new" do
    get new_client_etat_civil_url
    assert_response :success
  end

  test "should create client_etat_civil" do
    assert_difference('ClientEtatCivil.count') do
      post client_etat_civils_url, params: { client_etat_civil: { Client_id_id: @client_etat_civil.Client_id_id, DateDebut: @client_etat_civil.DateDebut, DateFin: @client_etat_civil.DateFin, EtatCivil_id_id: @client_etat_civil.EtatCivil_id_id } }
    end

    assert_redirected_to client_etat_civil_url(ClientEtatCivil.last)
  end

  test "should show client_etat_civil" do
    get client_etat_civil_url(@client_etat_civil)
    assert_response :success
  end

  test "should get edit" do
    get edit_client_etat_civil_url(@client_etat_civil)
    assert_response :success
  end

  test "should update client_etat_civil" do
    patch client_etat_civil_url(@client_etat_civil), params: { client_etat_civil: { Client_id_id: @client_etat_civil.Client_id_id, DateDebut: @client_etat_civil.DateDebut, DateFin: @client_etat_civil.DateFin, EtatCivil_id_id: @client_etat_civil.EtatCivil_id_id } }
    assert_redirected_to client_etat_civil_url(@client_etat_civil)
  end

  test "should destroy client_etat_civil" do
    assert_difference('ClientEtatCivil.count', -1) do
      delete client_etat_civil_url(@client_etat_civil)
    end

    assert_redirected_to client_etat_civils_url
  end
end
