require 'test_helper'

class EtatCivilsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @etat_civil = etat_civils(:one)
  end

  test "should get index" do
    get etat_civils_url
    assert_response :success
  end

  test "should get new" do
    get new_etat_civil_url
    assert_response :success
  end

  test "should create etat_civil" do
    assert_difference('EtatCivil.count') do
      post etat_civils_url, params: { etat_civil: { type: @etat_civil.type } }
    end

    assert_redirected_to etat_civil_url(EtatCivil.last)
  end

  test "should show etat_civil" do
    get etat_civil_url(@etat_civil)
    assert_response :success
  end

  test "should get edit" do
    get edit_etat_civil_url(@etat_civil)
    assert_response :success
  end

  test "should update etat_civil" do
    patch etat_civil_url(@etat_civil), params: { etat_civil: { type: @etat_civil.type } }
    assert_redirected_to etat_civil_url(@etat_civil)
  end

  test "should destroy etat_civil" do
    assert_difference('EtatCivil.count', -1) do
      delete etat_civil_url(@etat_civil)
    end

    assert_redirected_to etat_civils_url
  end
end
