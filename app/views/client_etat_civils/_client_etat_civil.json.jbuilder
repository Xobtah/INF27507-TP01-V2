json.extract! client_etat_civil, :id, :Client_id_id, :EtatCivil_id_id, :DateDebut, :DateFin, :created_at, :updated_at
json.url client_etat_civil_url(client_etat_civil, format: :json)
