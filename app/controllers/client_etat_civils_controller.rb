class ClientEtatCivilsController < ApplicationController
  before_action :set_client_etat_civil, only: [:show, :edit, :update, :destroy]

  # GET /client_etat_civils
  # GET /client_etat_civils.json
  def index
    @client_etat_civils = ClientEtatCivil.all
  end

  # GET /client_etat_civils/1
  # GET /client_etat_civils/1.json
  def show
  end

  # GET /client_etat_civils/new
  def new
    @client_etat_civil = ClientEtatCivil.new
  end

  # GET /client_etat_civils/1/edit
  def edit
  end

  # POST /client_etat_civils
  # POST /client_etat_civils.json
  def create
    @client_etat_civil = ClientEtatCivil.new(client_etat_civil_params)

    respond_to do |format|
      if @client_etat_civil.save
        format.html { redirect_to @client_etat_civil, notice: 'Client etat civil was successfully created.' }
        format.json { render :show, status: :created, location: @client_etat_civil }
      else
        format.html { render :new }
        format.json { render json: @client_etat_civil.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /client_etat_civils/1
  # PATCH/PUT /client_etat_civils/1.json
  def update
    respond_to do |format|
      if @client_etat_civil.update(client_etat_civil_params)
        format.html { redirect_to @client_etat_civil, notice: 'Client etat civil was successfully updated.' }
        format.json { render :show, status: :ok, location: @client_etat_civil }
      else
        format.html { render :edit }
        format.json { render json: @client_etat_civil.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /client_etat_civils/1
  # DELETE /client_etat_civils/1.json
  def destroy
    @client_etat_civil.destroy
    respond_to do |format|
      format.html { redirect_to client_etat_civils_url, notice: 'Client etat civil was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client_etat_civil
      @client_etat_civil = ClientEtatCivil.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_etat_civil_params
      params.require(:client_etat_civil).permit(:Client_id_id, :EtatCivil_id_id, :DateDebut, :DateFin)
    end
end
