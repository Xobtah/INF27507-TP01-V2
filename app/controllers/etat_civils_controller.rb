class EtatCivilsController < ApplicationController
  before_action :set_etat_civil, only: [:show, :edit, :update, :destroy]

  # GET /etat_civils
  # GET /etat_civils.json
  def index
    @etat_civils = EtatCivil.all
  end

  # GET /etat_civils/1
  # GET /etat_civils/1.json
  def show
  end

  # GET /etat_civils/new
  def new
    @etat_civil = EtatCivil.new
  end

  # GET /etat_civils/1/edit
  def edit
  end

  # POST /etat_civils
  # POST /etat_civils.json
  def create
    @etat_civil = EtatCivil.new(etat_civil_params)

    respond_to do |format|
      if @etat_civil.save
        format.html { redirect_to @etat_civil, notice: 'Etat civil was successfully created.' }
        format.json { render :show, status: :created, location: @etat_civil }
      else
        format.html { render :new }
        format.json { render json: @etat_civil.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /etat_civils/1
  # PATCH/PUT /etat_civils/1.json
  def update
    respond_to do |format|
      if @etat_civil.update(etat_civil_params)
        format.html { redirect_to @etat_civil, notice: 'Etat civil was successfully updated.' }
        format.json { render :show, status: :ok, location: @etat_civil }
      else
        format.html { render :edit }
        format.json { render json: @etat_civil.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /etat_civils/1
  # DELETE /etat_civils/1.json
  def destroy
    @etat_civil.destroy
    respond_to do |format|
      format.html { redirect_to etat_civils_url, notice: 'Etat civil was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_etat_civil
      @etat_civil = EtatCivil.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def etat_civil_params
      params.require(:etat_civil).permit(:type)
    end
end
