rails generate scaffold Client Prenom:string Nom:string \
DateNaissance:datetime NAS:string Adresse:references \
NombreEnfants:integer CompteTaxesProprietaire:integer;

rails generate model Client_has_Conjoint Client:references \
Client_conjoint:references DateDebut:datetime DateFin:datetime;

rails generate model Client_has_Enfant Enfant:references Client:references \
Lien:integer;

rails generate scaffold Enfant Nom:string Prenom:string \
DateNaissance:datetime;

rails generate scaffold Etude SecteurEtude:string Niveau:string \
DateDebut:datetime DateComplition:datetime Client:references \
Institution:references;

rails generate scaffold Institution Nom:string Adresse:references;

rails generate scaffold Adresse NumeroCivique:string Rue:string \
CodePostal:string Ville:string Province:string;

rails generate model Client_has_Employeur Client:references Employeur:references \
DateDebut:datetime DateFin:datetime;

rails generate scaffold Employeur nom:string Adresse:references;

rails generate scaffold Client_EtatCivil Client:references EtatCivil:references \
DateDebut:datetime DateFin:datetime;

rails generate scaffold EtatCivil type:string;

rake db:migrate
