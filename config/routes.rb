Rails.application.routes.draw do

  resources :etat_civils
  resources :client_etat_civils
  resources :employeurs
  resources :adresses
  resources :institutions
  resources :etudes
  resources :enfants
  resources :clients
  root 'clients#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
