rails generate scaffold Client Prenom:string Nom:string \
DateNaissance:datetime NAS:string Adresse_id:references \
NombreEnfants:integer CompteTaxesProprietaire:integer;

rails generate model Client_has_Conjoint Client_id:references \
Client_conjoint_id:references DateDebut:datetime DateFin:datetime;

rails generate model Client_has_Enfant Enfant_id:references Client_id:references \
Lien:integer;

rails generate scaffold Enfant Nom:string Prenom:string \
DateNaissance:datetime;

rails generate scaffold Etude SecteurEtude:string Niveau:string \
DateDebut:datetime DateComplition:datetime Client_id:references \
Institution_id:references;

rails generate scaffold Institution Nom:string Adresse_id:references;

rails generate scaffold Adresse NumeroCivique:string Rue:string \
CodePostal:string Ville:string Province:string;

rails generate model Client_has_Employeur Client_id:references Employeur_id:references \
DateDebut:datetime DateFin:datetime;

rails generate scaffold Employeur nom:string Adresse_id:references;

rails generate scaffold Client_EtatCivil Client_id:references EtatCivil_id:references \
DateDebut:datetime DateFin:datetime;

rails generate scaffold EtatCivil type:string;

rake db:migrate
